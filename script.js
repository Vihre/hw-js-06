function createNewUser () {
  this.firstName = prompt("Add your firstName");
  while (this.firstName === "") {
      this.firstName = prompt("Add your firstName");
  }
  this.lastName = prompt("Add your lastName");
  while (this.lastName === "") {
      this.lastName = prompt("Add your lastName");
  }
  this.birthday = prompt("Add your birthday date (dd.mm.yyyy)", "dd.mm.yyyy");
  while (this.birthday === "") {
      this.birthday = prompt("Add your birthday date (dd.mm.yyyy)", "dd.mm.yyyy");
  }
  this.getLogin = function () {
      let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return newLogin;
  }
  this.getPassword = function () {
      let newPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
      return newPassword;
  }
  this.getAge = function () {
      let reversDate = this.birthday.split('.').reverse().join('.');
      return +((new Date().getTime() - new Date(reversDate)) / (24 * 3600 * 365.25 * 1000)) | 0;
  }
}

let newObj = new createNewUser();
console.log(`${newObj.getPassword()}`);
console.log(`${newObj.getAge()}`);
console.log(newObj);